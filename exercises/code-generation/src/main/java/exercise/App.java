package exercise;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Files;
import java.util.Collections;

// BEGIN
public class App {
    public static void save(Path path, Car car) throws IOException {
        String json = car.serialize();
        Files.write(path, Collections.singleton(json));
    }

    public static Car extract(Path path) throws IOException {
        File json = path.toFile();
        return Car.unserialize(String.valueOf(json));
    }
}
// END
