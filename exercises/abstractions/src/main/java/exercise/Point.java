package exercise;
import java.util.Arrays;


class Point {
    public static void main(String[] args) {
//        int[] array1 = {3, 4};
//        int[] result = getSymmetricalPointByX(array1);
//        System.out.println(Arrays.toString(result));

        int[] array1 = {0, 0};
        int[] array2 = {3, 4};
        System.out.println(calculateDistance(array1, array2));

    }
    // FOR DEBUG

    // BEGIN
    public static int[] makePoint(int pointX, int pointY) {
        return new int[] {pointX, pointY};
    }

    public static int getX(int[] point) {
        return point[0];
    }

    public static int getY(int[] point) {
        return point[1];
    }

    public static String pointToString(int[] pointInInt) {
        String pointToString = Arrays.toString(pointInInt);
        pointToString = pointToString.replaceAll("^.", "(");
        pointToString = pointToString.replaceAll(".$", ")");

        return pointToString;
    }

    // scheme for getQuadrant()
    /*
        2                           1
        X {NEGATIVE_INFINITY, 0}    X {0, POSITIVE_INFINITY}
        Y {0, POSITIVE_INFINITY}    Y {0, POSITIVE_INFINITY}

        3                           4
        X {NEGATIVE_INFINITY, 0}    X {0, POSITIVE_INFINITY}
        Y {0, NEGATIVE_INFINITY}    Y {0, NEGATIVE_INFINITY}
     */

    public static int getQuadrant(int[] point) {
        int pointX = Point.getX(point);
        int pointY = Point.getY(point);

        if (pointX > 0 && pointY > 0) {
            return 1;
        } else if (pointX < 0 && pointY > 0) {
            return 2;
        } else if (pointX < 0 && pointY < 0) {
            return 3;
        } else if (pointX > 0 && pointY < 0) {
            return 4;
        } else {
            return 0;
        }
    }
    // END OF TASK


    public static int[] getSymmetricalPointByX(int[] point) {
            point[0] = point[0] - (point[0] * 2); // какой более правильный способ существует?
            return point;
    }
    // END OF TASK

    public static double calculateDistance(int[] point1, int[] point2) {
        int pointX1 = Point.getX(point1);
        int pointY1 = Point.getY(point1);

        int pointX2 = Point.getX(point2);
        int pointY2 = Point.getY(point2);

        double diffX = Math.pow(pointX2 - pointX1, 2);
        double diffY = Math.pow(pointY2 - pointY1, 2);

        return Math.sqrt(diffX + diffY);
    }
    //END
}
