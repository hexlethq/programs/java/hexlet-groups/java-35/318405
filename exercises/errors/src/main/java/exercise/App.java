package exercise;

import exercise.exception.NegativeRadiusException;

// BEGIN
public class App {
    public static void printSquare(Circle circle) {
        try {
            int roundedSquare = (int) Math.ceil(circle.getSquare());
            System.out.println(roundedSquare);
        } catch (NegativeRadiusException e) {
            System.out.println(e.getMessage());
        } finally {
            System.out.println("Вычисление окончено");
        }
    }
}
// END
