package exercise;

import exercise.exception.CustomExceptions;
import exercise.exception.NegativeRadiusException;

// BEGIN
public class Circle {
    private final Point point;
    private final int radius;

    public Circle(Point point, int radius) {
        this.point = point;
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    public double getSquare() throws NegativeRadiusException {
        if (this.radius < 0) {
            throw CustomExceptions.NEGATIVE_RADIUS_EXCEPTION;
        } else {
            return Math.PI * Math.pow(this.getRadius(), 2);
        }
    }
}
// END
