// BEGIN
package exercise;

import com.google.gson.Gson;

class App {
    public static void main(String[] args) {
        System.out.println("Hello, World!");

//        String[] fruits = {"apple", "pear", "lemon"};
//        System.out.println(App.toJson(fruits)); // => ["apple","pear","lemon"]
    }

    public static String toJson(String[] fruits) {
        return new Gson().toJson(fruits);
    }
}
// END
