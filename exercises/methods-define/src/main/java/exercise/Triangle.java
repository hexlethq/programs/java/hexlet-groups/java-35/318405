package exercise;

class Triangle {
    // BEGIN
    public static void main(String[] args) {
        System.out.println(getSquare(4, 5, 45));
    }

    public static double getSquare(int side1, int side2, int angle) {
        double fromDegreesToRadian = (angle * Math.PI) / 180;
        double angleSin = Math.sin(fromDegreesToRadian);
        double sTriangle = (side1 * side2) / 2 * angleSin;
        return sTriangle;
    }
    // END
}
