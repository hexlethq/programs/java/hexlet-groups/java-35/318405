package exercise;

class Converter {
    // BEGIN
    public static void main() {
        int bytesCount = convert(10, "b");
        System.out.println("10 Kb = " + bytesCount + " b");
    }

    public static int convert(int size, String sizeType) {
        if (sizeType.equals("Kb")) {
            size = size / 1024;
            return size;
        } else if (sizeType.equals("b")) {
            size = size * 1024;
            return size;
        } else {
            return 0;
        }
    }
    // END
}
