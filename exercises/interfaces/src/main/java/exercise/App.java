package exercise;

import java.util.List;
import java.util.stream.Collectors;

// BEGIN
public class App {

	public static List<String> buildAppartmentsList(List<Home> appartments, int numberOfPrinted) {
		return appartments.stream()
				.sorted(Home::compareTo)
				.limit(numberOfPrinted)
				.map(appartment -> appartment.toString())
				.collect(Collectors.toList());
	}
}
// END
