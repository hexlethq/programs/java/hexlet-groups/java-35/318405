package exercise;

import java.lang.String;

// BEGIN
public class ReversedSequence implements CharSequence {

	String sequence;

	public ReversedSequence(String text) {
		this.sequence = reverseString(text);
	}

	public static String reverseString(String text) {
		if (text == null) {
			return "String is null!";
		}

		String reversedString = "";
		char currentChar;

		for (int i = text.length() - 1; i >= 0; i--) {
			currentChar = text.charAt(i);
			reversedString += currentChar;
		}

		return reversedString;
	}

	@Override
	public int length() {
		return sequence.length();
	}

	@Override
	public char charAt(int index) {
		return sequence.charAt(index);
	}

	@Override
	public CharSequence subSequence(int start, int end) {
		return this.toString().subSequence(start, end);
	}

	@Override
	public String toString() {
		return sequence;
	}
}
// END
