package exercise;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class ReversedSequenceTest {

	CharSequence text = "abcd";

	@Test
	@DisplayName("Тест конструктора ReversedSequence.")
	void testReverseCharSequence1() {
		CharSequence constructorResult = new ReversedSequence("abcd");
		String result = constructorResult.toString();
		String expected = "dcba";

		assertThat(result).isEqualTo(expected);
	}

	@Test
	@DisplayName("Тест конструктора ReversedSequence при входящем null.")
	void testReverseCharSequence2() {
		CharSequence constructorResult = new ReversedSequence(null);
		String result = constructorResult.toString();
		String expected = "String is null!";

		assertThat(result).isEqualTo(expected);
	}

	@Test
	@DisplayName("Тест charAt().")
	void testReverseCharSequence3() {
		char result = text.charAt(1);
		char expected = 'b';

		assertThat(result).isEqualTo(expected);
	}

	@Test
	@DisplayName("Тест length().")
	void testReverseCharSequence5() {
		int result = text.length();
		int expected = 4;

		assertThat(result).isEqualTo(expected);
	}

	@Test
	@DisplayName("Тест subSequence().")
	void testReverseCharSequence6() {
		CharSequence result = text.subSequence(0, 2);
		CharSequence expected = "ab";

		assertThat(result).isEqualTo(expected);
	}
}
