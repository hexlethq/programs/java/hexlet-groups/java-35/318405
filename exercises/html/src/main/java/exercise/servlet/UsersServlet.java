package exercise.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import exercise.User;
import org.apache.commons.lang3.ArrayUtils;

public class UsersServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        String pathInfo = request.getPathInfo();

        if (pathInfo == null) {
            showUsers(request, response);
            return;
        }

        String[] pathParts = pathInfo.split("/");
        String id = ArrayUtils.get(pathParts, 1, "");

        showUser(request, response, id);
    }

    private List getUsers() throws JsonProcessingException, IOException {
        // BEGIN
        File file = new File("src/main/resources/users.json").getAbsoluteFile();
        ObjectMapper objectMapper = new ObjectMapper();

        return objectMapper.readValue(file, new TypeReference<List<User>>(){});
        // END
    }

    private void showUsers(HttpServletRequest request,
                          HttpServletResponse response)
                throws IOException {

        // BEGIN
        List<User> users = getUsers();

        StringBuilder body = new StringBuilder();
        body.append("<table>");

        for (User user : users) {
            String id = user.getId();
            String fullName = user.getFirstName() + " " + user.getLastName();

            body.append("<tr>")
                .append("<td>" + id + "</td>")

                .append("<td><a href=/users/").append(id + ">")
                .append(fullName).append("</a></td>")
            .append("</tr>");
        }

        body.append("</table>");

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println(body.toString());
        // END
    }

    private void showUser(HttpServletRequest request,
                         HttpServletResponse response,
                         String id)
                 throws IOException {

        // BEGIN
        User userById = null;

        if (!id.isEmpty()) {
            userById = getUserById(getUsers(), Integer.parseInt(id));
        }
        if (userById == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND, "Not found");
            return;
        }

        StringBuilder body = new StringBuilder();
        body.append("<table>")
            .append("<tr><td>" + userById.getFirstName() + "</td></tr>")
            .append("<tr><td>" + userById.getLastName() + "</td></tr>")
            .append("<tr><td>" + id + "</td></tr>")
            .append("<tr><td>" + userById.getEmail()+ "</td></tr>")
        .append("</table>");

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println(body.toString());
        // END
    }


    private User getUserById(List<User> users, int id) {
        User userById = null;

        for (User user : users) {
            int userId = Integer.parseInt(user.getId());
            if (userId == id) {
                userById = user;
            }
        }

        return userById;
    }

}
