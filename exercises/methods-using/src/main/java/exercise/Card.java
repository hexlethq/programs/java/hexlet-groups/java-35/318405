package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        String returnStars = "*".repeat(starsCount);
        String returnNumber = cardNumber.substring(12, 16);
        System.out.println(returnStars + returnNumber);
        // END
    }
}