package exercise;

class Sentence {
    public static void printSentence(String sentence) {
        // BEGIN
        int sentenceLength = sentence.length();
        char lastSign = sentence.charAt(sentenceLength -1);
        char examinSign = "!".charAt(0);
        if (lastSign == examinSign) {
            System.out.println(sentence.toUpperCase());
        } else {
            System.out.println(sentence.toLowerCase());
        }
        // END
    }
}