package exercise;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class AppTest {

    @Test
    void testTake() {
        // BEGIN
        List<Integer> elements = new ArrayList<>();
        elements.add(1);
        elements.add(2);

        List<Integer> actualEmpty = App.take(elements, 0);
        List<Integer> expectedEmpty = new ArrayList<>();
        Assertions.assertEquals(expectedEmpty, actualEmpty);

        List<Integer> actual1 = App.take(elements, 1);
        List<Integer> expected1 = List.of(1);
        Assertions.assertEquals(expected1, actual1);

        List<Integer> actual2 = App.take(elements, 2);
        List<Integer> expected2 = List.of(1, 2);
        Assertions.assertEquals(expected2, actual2);

        List<Integer> actual3 = App.take(elements, 3);
        List<Integer> expected3 = List.of(1, 2);
        Assertions.assertEquals(expected3, actual3);
        // END
    }
}
