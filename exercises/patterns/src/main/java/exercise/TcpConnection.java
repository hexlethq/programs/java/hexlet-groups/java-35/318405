package exercise;
import exercise.connections.Connected;
import exercise.connections.Connection;
import exercise.connections.Disconnected;

// BEGIN

public class TcpConnection {
    private String ip;
    private int port;
    public Connection connection;

    public TcpConnection(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    public void connect() {
        this.connection = new Connected(this);
        connection.connect();
    }

    public void disconnect() {
        this.connection = new Disconnected(this);
        connection.disconnect();
    }

    public void write(String data) {
        connection.write(data);
    }

    public String getCurrentState() {
        return this.connection.getCurrentState();
    }
}
// END
