package exercise.connections;

import exercise.TcpConnection;

// BEGIN
public class Connected implements Connection {
    public TcpConnection connection;

    public Connected(TcpConnection connection) {
        this.connection = connection;
    }

    @Override
    public String getCurrentState() {
        return "connected";
    }

    @Override
    public void connect() {
        System.out.println("Error");
    }

    @Override
    public void disconnect() {
        connection.disconnect();
    }

    @Override
    public void write(String data) { }
}
// END
