package exercise;

class App {
    // BEGIN
    public static String getTypeOfTriangle(int side1, int side2, int side3) {
        if  (side1 + side2 > side3
                && side1 + side3 > side2
                && side2 + side3 > side1) { // Треугольник существует
            if (side1 == side2
                    && side1 == side3
                    && side2 == side3) {
                return "Равносторонний";
            } else if (side1 != side2
                    && side1 != side3
                    && side2 != side3) {
                return "Разносторонний";
            } else if (side1 == side2
                    || side1 == side3
                    || side2 == side3) {
                return "Равнобедренный";
            }
        } return "Треугольник не существует";
    }
    // END
}