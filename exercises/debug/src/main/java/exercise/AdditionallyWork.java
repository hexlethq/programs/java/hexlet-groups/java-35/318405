package exercise;

public class AdditionallyWork {
    public static void main(String[] args) {
        getFinalGrade(80, 10);
    }

    public static int getFinalGrade(int exam, int project) {
        if  (project >= 0 && exam >= 0 && exam <= 100) {
            if (exam > 90 || project > 10) {
                return 100;
            } else if (exam > 75 && project >= 5) {
                return 90;
            } else if (exam > 50 && project >= 2) {
                return 75;
            }
        } return 0;
    }
}