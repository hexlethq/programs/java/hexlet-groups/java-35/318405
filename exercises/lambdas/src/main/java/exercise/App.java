package exercise;

import java.util.Arrays;
import java.util.stream.Stream;

// BEGIN
class App {
    public static void main(String[] args) {
        //Debug task 1
        String[][] image = {{"*", "*", "*", "*"}, {"*", " ", " ", "*"}, {"*", " ", " ", "*"}, {"*", "*", "*", "*"},};
//        App.enlargeArrayImage(image); // for print void enlargeArrayImage()
        System.out.println(Arrays.deepToString(image) + "\n");
        String[][] enlargedImage = App.enlargeArrayImage(image);
        System.out.println(Arrays.deepToString(enlargedImage));

        //Debug getDoubledArray()
//        String[] array = {"*", " ", "*"};
//        String[] doubledArray = getDoubledArray(array);
//        System.out.println(Arrays.toString(doubledArray));
    }

    public static String[][] enlargeArrayImage(String[][] matrix) {
        return Arrays.stream(matrix)
                .flatMap(element -> Stream.of(getDoubledExtendedArray(element)))
                .toArray(String[][]::new);
    }

    public static String[][] getDoubledExtendedArray(String[] array) {
        String[] extendedArray = Arrays.stream(array)
                .flatMap(element -> Stream.of(element, element))
                .toArray(String[]::new);

        return new String[][]{extendedArray, extendedArray};
    }
}
// END
