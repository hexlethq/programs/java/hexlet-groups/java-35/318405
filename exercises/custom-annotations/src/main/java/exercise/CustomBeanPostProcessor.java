package exercise;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

import exercise.calculator.Calculator;
import exercise.calculator.CalculatorImpl;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


// BEGIN
@Component
public class CustomBeanPostProcessor implements BeanPostProcessor {
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomBeanPostProcessor.class);

	private String loggingLevel;
	private Object annotatedBean;

	@Override
    public Object postProcessBeforeInitialization(Object bean, String beanName)
        throws BeansException {

        if (bean.getClass().isAnnotationPresent(Inspect.class)) {
            loggingLevel = bean.getClass().getAnnotation(Inspect.class).level();
	        annotatedBean = bean;
        }

	    return BeanPostProcessor.super.postProcessBeforeInitialization(bean, beanName);
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName)
        throws BeansException {
	    if (Objects.equals(annotatedBean, bean)) {
            return Proxy.newProxyInstance(
                CalculatorImpl.class.getClassLoader(),
                CalculatorImpl.class.getInterfaces(),
                getInvocationHandler(annotatedBean)
            );
	    }

	    return BeanPostProcessor.super.postProcessAfterInitialization(bean, beanName);
    }


    private InvocationHandler getInvocationHandler(Object bean) {
        return (proxy, method, args) -> {
	        String msg = "Was called method: {}() with arguments: {}";

	        if (loggingLevel.equals("info")) {
		        LOGGER.info(msg, method.getName(), Arrays.toString(args));
	        }
	        if (loggingLevel.equals("debug")) {
		        LOGGER.debug(msg, method.getName(), Arrays.toString(args));
	        } else {
		        LOGGER.error("Wrong loggingLevel name in this bean: " + bean.getClass().getName());
	        }

	        return method.invoke(bean, args);
        };
    }

}
// END
