package exercise;

import java.util.Map;

// BEGIN
public abstract class Tag {
    String tagName;
    Map<String, String> attributes;

    public abstract String toString();

    public String getOneTag(String tagName, Map<String, String> attributes) {
        StringBuilder result = new StringBuilder();

        result.append("<" + tagName + " ");

        for (Map.Entry<String, String> map : attributes.entrySet()) {
            result.append(map.getKey() + "=" + "\"" + map.getValue() + "\" ");
        }

        result.setLength(result.length() - 1);
        result.append(">");

        return result.toString();
    }


    public String getTagName() {
        return tagName;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }
}
// END
