package exercise;

import java.util.Map;
import java.util.List;

// BEGIN
public class PairedTag extends Tag {
    private final String tagBody;
    private final List<Tag> tagsInner;

    public PairedTag(String tagName, Map<String, String> attributes, String tagBody, List<Tag> tagInner) {
        this.tagBody = tagBody;
        this.tagsInner = tagInner;
        this.tagName = tagName;
        this.attributes = attributes;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        String oneTag = getOneTag(tagName, attributes);

        result.append(oneTag)
            .append(tagBody)
            .append(getTags(this.tagsInner))
            .append("</" + tagName + ">");

        return result.toString();
    }

    public String getTags(List<Tag> tags) {
        StringBuilder result = new StringBuilder();

        for (Tag tag: tags) {
            result.append(
                getOneTag(tag.getTagName(), tag.getAttributes())
            );
        }

        return result.toString();
    }
}
// END
