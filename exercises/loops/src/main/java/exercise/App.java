package exercise;

class App {
//    public static void main(String[] args) {
//        System.out.println("return: " + getAbbreviation("  Portable network   Graphics   "));
//    }
    // BEGIN
    public static String getAbbreviation(String str) {
        str = str.strip(); // delete space of beginning and end
        str = str.replaceAll("\\s+", " "); // delete space of middle

        char firstChar = str.charAt(0);
        char controlChar = ' ';
        String resultString = Character.toString(firstChar);

        for (int counter = 0; counter <= str.length() - 1; ++counter) {
            char checkCurrentChar = str.charAt(counter);
            if (controlChar == checkCurrentChar) {
                char charAfterSpace = str.charAt(counter + 1);
                resultString = resultString + charAfterSpace;
            }
        } return resultString.toUpperCase();
    }
    // END
}
