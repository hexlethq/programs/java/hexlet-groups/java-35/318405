package exercise;
import java.util.Arrays;

class App {
    // BEGIN
    public static void main(String[] args) {
        int[] numbers1 = {3, 10, 4, 3};
        int[] numbers2 = {10, 3, 4, 3, 23, 1, 7};
        int[] sorted = App.sort2(numbers2);
        System.out.println(Arrays.toString(sorted)); // => [3, 3, 4, 10]
    }
    // FOR DEBUG

    public static int[] sort(int[] numbers) {
        boolean sortedStatus = false;
        while (!sortedStatus) {
            sortedStatus = true;
            for (int i = 1; i < numbers.length; i++) {
                if (numbers[i] < numbers[i - 1]) {
                    int previousElement = numbers[i];
                    numbers[i] = numbers[i - 1];
                    numbers[i - 1] = previousElement;
                    sortedStatus = false;
                }
            }
        }
        return numbers;
    }
    // END OF TASK

    public static int[] sort2(int[] numbers) {
        for (int iteration = 0; iteration < numbers.length; iteration++) {
            // выявляю минимальный элемент внутри одной итерации
            int minIndex = iteration;
            int minValue = numbers[iteration];
            for (int i = iteration + 1; i < numbers.length; i++) {
                if (minValue > numbers[i]) {
                    minValue = numbers[i];
                    minIndex = i;
                }
            }
            // меняю местами минимальный и текущий элементы
            int tmp = numbers[iteration];
            numbers[iteration] = numbers[minIndex];
            numbers[minIndex] = tmp;
        }
        return numbers;
    }
    // END OF TASK
}