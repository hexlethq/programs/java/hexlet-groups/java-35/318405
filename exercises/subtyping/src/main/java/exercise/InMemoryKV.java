package exercise;

import java.util.Map;
import java.util.HashMap;
import java.util.stream.Collectors;

// BEGIN
public class InMemoryKV implements KeyValueStorage {
	public Map<String, String> kV;

	public InMemoryKV(Map<String, String> kV) {
		this.kV = new HashMap<>(kV);
	}

	@Override
	public void set(String key, String value) {
		kV.put(key, value);
	}

	@Override
	public void unset(String key) {
		kV.remove(key);
	}

	@Override
	public String get(String key, String defaultValue) {
		return kV.getOrDefault(key, defaultValue);
	}

	@Override
	public Map<String, String> toMap() {
		return new HashMap<>(kV);
	}
}
// END
