package exercise;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

// BEGIN
public class App {
	public static void swapKeyValue(KeyValueStorage kV) {
		Map<String, String> tmpStorage = kV.toMap();

		for (Map.Entry<String, String> pair : tmpStorage.entrySet()) {
			kV.unset(pair.getKey());
		}

		for (Map.Entry<String, String> pair : tmpStorage.entrySet()) {
			kV.set(pair.getValue(), pair.getKey());
		}
	}
}
// END
