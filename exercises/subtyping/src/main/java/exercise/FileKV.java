package exercise;

import exercise.Utils.*;
import java.util.HashMap;
import java.util.Map;

// BEGIN
public class FileKV implements KeyValueStorage {
	private final String filePath;
    private Map<String, String> dataBase = new HashMap<>();

	public FileKV(String filePath, Map<String, String> dataBase) {
		this.filePath = filePath;
		String storageAsString = Utils.serialize(dataBase);
		Utils.writeFile(filePath, storageAsString);
	}

	@Override
	public void set(String key, String value) {
        String json = Utils.readFile(filePath);
        dataBase = Utils.unserialize(json);

        dataBase.put(key, value);
	}

	@Override
	public void unset(String key) {
        String json = Utils.readFile(filePath);
        dataBase = Utils.unserialize(json);

        dataBase.remove(key);

        String jsonOut = Utils.serialize(dataBase);
        Utils.writeFile(filePath, jsonOut);
	}

	@Override
	public String get(String key, String defaultValue) {
        String json = Utils.readFile(filePath);
        dataBase = Utils.unserialize(json);

		return dataBase.getOrDefault(key, defaultValue);
	}

	@Override
	public Map<String, String> toMap() {
        String json = Utils.readFile(filePath);
        dataBase = Utils.unserialize(json);

		return new HashMap<>(dataBase);
	}
}
// END
