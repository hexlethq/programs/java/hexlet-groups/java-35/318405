package exercise;

import java.util.HashMap;
import org.junit.jupiter.api.BeforeEach;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import com.fasterxml.jackson.databind.ObjectMapper;
// BEGIN
import java.util.Map;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
// END


class FileKVTest {
    private static Path filepath = Paths.get("src/test/resources/file").toAbsolutePath().normalize();
    private static String path = filepath.toString();

    @BeforeEach
    public void beforeEach() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String content = mapper.writeValueAsString(new HashMap<String, String>());
        Files.writeString(filepath, content, StandardOpenOption.CREATE);
    }

    // BEGIN
    @Test
    void testGet() {
        KeyValueStorage storage = new FileKV(path, Map.of("key", "value"));

        String actual = storage.get("key", "default");
        assertThat(actual).isEqualTo("value");
    }

    @Test
    void testSet() {
        Map<String, String> map = new HashMap<>();
        map.put("key", "value");
        map.put("key2", "value2");
        KeyValueStorage storage = new FileKV(path, map);

        storage.set("key2", "value2");

        Map<String, String> expected = Map.of("key", "value", "key2", "value2");
        assertThat(storage.toMap()).isEqualTo(expected);
    }

    @Test
    void testUnset() {
        Map<String, String> map = new HashMap<>();
        map.put("key", "value");
        map.put("key2", "value2");
        KeyValueStorage storage = new FileKV(path, map);

        storage.unset("key");

        Map<String, String> expected = Map.of("key2", "value2");
        assertThat(storage.toMap()).isEqualTo(expected);
    }
    // END
}


