package exercise.controller;
import com.fasterxml.jackson.core.JsonProcessingException;
import exercise.CityNotFoundException;
import exercise.model.City;
import exercise.repository.CityRepository;
import exercise.service.WeatherService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class CityController {

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private WeatherService weatherService;

    // BEGIN
    @GetMapping(path = "/cities/{id}")
    public Map<String, String> getWeatherInCity(@PathVariable Long id) throws JsonProcessingException {
        return weatherService.lookUp(id);
    }

    @GetMapping(path = "/search")
    public List<Map<String, String>> getTemperaturByCityNamePrefix(@RequestParam(value = "name", defaultValue = "") String prefix) {

        if (prefix.isEmpty()) {
            return cityRepository.findAllByOrderByName().stream()
                .map(this::getCitiesWithTemperature)
                .collect(Collectors.toList());
        }

        return cityRepository.findCitiesByNameStartingWithIgnoreCase(prefix).stream()
            .map(this::getCitiesWithTemperature)
            .collect(Collectors.toList());
    }


    private Map<String, String> getCitiesWithTemperature(City city) {
        Map<String, String> weatherJson;
        try {
            weatherJson = weatherService.lookUp(city.getId());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        String temperature = weatherJson.get("temperature");

        return Map.of(
            "temperature", temperature,
            "name", city.getName());
    }
    // END
}

