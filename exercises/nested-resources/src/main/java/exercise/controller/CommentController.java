package exercise.controller;

import exercise.model.Comment;
import exercise.repository.CommentRepository;
import exercise.model.Post;
import exercise.repository.PostRepository;
import exercise.ResourceNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PatchMapping;


@RestController
@RequestMapping("/posts")
public class CommentController {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private PostRepository postRepository;

    // BEGIN
    @GetMapping(path = "/{postId}/comments")
    public Iterable<Comment> getAllCommentsByPost(@PathVariable Long postId) {
        return commentRepository.findAllByPostId(postId);
    }

    @GetMapping(path = "/{postId}/comments/{commentId}")
    public Comment getCommentByPost(@PathVariable Long postId, @PathVariable Long commentId) {
        return commentRepository.findByIdAndPostId(commentId, postId)
            .orElseThrow(() -> new ResourceNotFoundException("Comment " + commentId + " not found"));
    }

    @PostMapping(path = "/{postId}/comments")
    public void createCommentByPost(@RequestBody Comment comment, @PathVariable Long postId) {
        Post post = postRepository.findById(postId)
            .orElseThrow(() -> new ResourceNotFoundException("Post " + postId + " not found"));
        comment.setPost(post);

        commentRepository.save(comment);
    }

    @PatchMapping(path = "/{postId}/comments/{commentId}")
    public Comment updateCommentByPost(@RequestBody Comment newComment,
        @PathVariable Long postId,
        @PathVariable Long commentId) {

        Comment outdatedComment = commentRepository.findByIdAndPostId(commentId, postId)
            .orElseThrow(() -> new ResourceNotFoundException("Comment or post not found"));

        outdatedComment.setContent(newComment.getContent());

        return commentRepository.save(outdatedComment);
    }

    @DeleteMapping(path = "/{postId}/comments/{commentId}")
    public void deleteCommentByPost(@PathVariable Long postId, @PathVariable Long commentId) {
        Comment comment = commentRepository.findByIdAndPostId(commentId, postId)
            .orElseThrow(() -> new ResourceNotFoundException("Comment " + commentId + " not found"));

        commentRepository.delete(comment);
    }
    // END
}
