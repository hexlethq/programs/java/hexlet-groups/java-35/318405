package exercise;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

//BEGIN
class App {

    public static boolean scrabble(String charSet, String word) {
        // Make strings to lower case
        charSet = charSet.toLowerCase();
        word = word.toLowerCase();

        // Make method arguments as a list of characters
        List<Character> wordAsList = new ArrayList<>(convertStringToCharList(word));
        List<Character> symbolsAsList = new ArrayList<>(convertStringToCharList(charSet));

        return isListEntryIntoAnotherList(wordAsList, symbolsAsList);
    }

    /**
     * Convert string to chars list
     */
    public static List<Character> convertStringToCharList(String str) {
        List<Character> sharsOfStr = new ArrayList<>();

        // Convert chars array to chars list
        for (char ch: str.toCharArray()) {
            sharsOfStr.add(ch);
        }

        return sharsOfStr;
    }

    /**
     * Check entry chars of array into another array
     */
    public static boolean isListEntryIntoAnotherList(List<Character> checkableList, List<Character> list) {
        if (list.size() < checkableList.size()) {
            return false;
        }

        // remove any symbol that entry to checkableList
        for (Character ch: list) {
            checkableList.remove(ch);
        }

        if (checkableList.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}
//END
