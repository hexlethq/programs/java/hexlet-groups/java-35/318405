package exercise;

import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

// BEGIN
public class App {

    public static int getCountOfFreeEmails(List<String> emails) {
        List<String> freeDomains = new ArrayList<>();
            freeDomains.add("gmail.com");
            freeDomains.add("yandex.ru");
            freeDomains.add("hotmail.com");

        return (int) emails.stream()
                .map(email -> email.split("@"))
                .flatMap(Arrays::stream)
                .filter(freeDomains::contains)
                .count();
    }
}
// END
