package exercise;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;

// BEGIN
class App {
    public static void main(String[] args) {
        List<Map<String, String>> books = new ArrayList<>();
        Map<String, String> book1 = new HashMap<>(Map.of("title", "Cymbeline", "author", "Shakespeare", "year", "1611"));
        Map<String, String> book2 = new HashMap<>(Map.of("title", "Book of Fooos", "author", "FooBar", "year", "1111"));
        Map<String, String> book3 = new HashMap<>(Map.of("title", "The Tempest", "author", "Shakespeare", "year", "1611"));
        Map<String, String> book4 = new HashMap<>(Map.of("title", "Book of Foos Barrrs", "author", "FooBar", "year", "2222"));
        Map<String, String> book5 = new HashMap<>(Map.of("title", "Still foooing", "author", "FooBar", "year", "3333"));
        books.add(book1); books.add(book2); books.add(book3); books.add(book4); books.add(book5);

//        Map<String, String> where = new HashMap<>(Map.of("author", "Shakespeare", "year", "1611")); // test 1
        Map<String, String> where = Map.of("title", "Still foooing", "author", "FooBar", "year", "4444"); //test 3

        List<Map<String, String>> result = App.findWhere(books, where);
        System.out.println("\n" + result);
    }

    /**
     * Task: Return List with all books, whose data match the Map pairs
     */
    public static List<Map<String, String>> findWhere(List<Map<String, String>> books, Map<String, String> soughtDictionary) {
        List<Map<String, String>> foundBooks = new ArrayList<>();

        for (Map<String, String> book: books) {
            // check for match each values of soughtDictionary
            boolean isValuesMatch = true;
            for (Map.Entry<String, String> soughtPair: soughtDictionary.entrySet()) {
                if (book.containsValue(soughtPair.getValue())) {
                    isValuesMatch = true;
                } else {
                    isValuesMatch = false;
                    break;
                }
            }

            if (isValuesMatch) {
                foundBooks.add(book);
            }
        }

        return foundBooks;
    }
}
//END
