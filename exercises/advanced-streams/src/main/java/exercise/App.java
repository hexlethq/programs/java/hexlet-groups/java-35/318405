package exercise;

import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.Arrays;
import java.util.stream.Stream;

// BEGIN
class App {
    public static String getForwardedVariables(String content) {
        return Stream.of(content)
                .map(string -> string.split("\n"))
                .flatMap(Stream::of)

                .filter(string -> string.startsWith("environment"))
                .map(string -> string.split(","))
                .flatMap(Stream::of)

                .filter(string -> string.contains("X_FORWARDED_"))
                .map(string -> string.replaceAll(".+_FORWARDED_", ""))
                .map(string -> string.replaceAll("\"", ""))

                .collect(Collectors.joining(","));
    }
}
//END
