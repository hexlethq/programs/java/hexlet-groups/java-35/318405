// BEGIN
package exercise;

import java.util.Arrays;
import exercise.geometry.Point;
import exercise.geometry.Segment;

class App {
    public static void main(String[] args) {
        double[] point1 = Point.makePoint(3, 4);
        double[] point2 = Point.makePoint(6, 7);
        double[][] segment = Segment.makeSegment(point1, point2);

        // getMidpointOfSegment
        System.out.println(Arrays.toString(App.getMidpointOfSegment(segment)) + "\n"); // => [4.5, 5.5]

        // reverse
        System.out.println(Arrays.deepToString(reverse(segment)));

        // isBelongToOneQuadrant
        /*
        2    |   1
        X -  |   X +
        Y +  |   Y +
      ---------------
        3    |   4
        X -  |   X +
        Y -  |   Y -
        */
        double[] newPoint1 = Point.makePoint(3, 4); // квадрант 1
        double[] newPoint2 = Point.makePoint(-6, 7); // квадрант 2
        double[] newPoint3 = Point.makePoint(-3, -4); // квадрант 3
        double[] newPoint4 = Point.makePoint(6, -7); // квадрант 4
        double[][] newSegment = Segment.makeSegment(newPoint1, newPoint4);
        System.out.println(isBelongToOneQuadrant(newSegment));
    }
    // FOR DEBUG

    /**
     * Возвращает точку середины отрезка.
     */
    public static double[] getMidpointOfSegment(double[][] segment) {
        double modpointX = (segment[0][0] + segment[1][0]) / 2;
        double modpointY = (segment[0][1] + segment[1][1]) / 2;

        return new double[]{modpointX, modpointY};
    }

    /**
     * Возвращает новый отрезок с точками, добавленными в обратном порядке.
     */
    public static double[][] reverse(double[][] segment) {
        double[][] result = {segment[1].clone(), segment[0].clone()};
        // проверяю: элементы разные, или это ссылки
//        System.out.println(Arrays.hashCode(result[1]));
//        System.out.println(Arrays.hashCode(segment[1]));
        return result;

    }

    /**
     * Если начало и конец отрезка лежат в одном квадранте, метод возвращает `true`, иначе —  `false`.
     * Если хотя бы одна из точек лежит на оси координат, то считается, что отрезок не находится целиком в одном квадранте.
     */
    public static boolean isBelongToOneQuadrant(double[][] segment) {
        double[] beginPoint = Segment.getBeginPoint(segment);
        double[] endPoint = Segment.getEndPoint(segment);

        return Point.getQuadrant(beginPoint) == Point.getQuadrant(endPoint);
    }

}
// END
