// BEGIN
package exercise.geometry;

import java.util.Arrays;

public class Segment {
    public static void main(String[] args) {
        // вернуть отрезок
        double[] point1 = Point.makePoint(2, 3);
        double[] point2 = Point.makePoint(4, 5);
        double[][] segment = Segment.makeSegment(point1, point2);
        System.out.println(Arrays.deepToString(segment) + "\n");

        // getEndPoint
        double[] beginPoint = Segment.getBeginPoint(segment);
        System.out.println(Arrays.toString(beginPoint)); // => [2, 3]

        // getEndPoint
        double[] endPoint = Segment.getEndPoint(segment);
        System.out.println(Arrays.toString(endPoint)); // => [4, 5]
    }
    // FOR DEBUG

    /**
     * Возвращает отрезок.
     */
    public static double[][] makeSegment(double[] point1, double[] point2) {
        double[][] segment = {point1, point2};
        return segment;
    }

    /**
     * Возвращает точку начала отрезка.
     */
    public static double[] getBeginPoint(double[][] segment) {
        return segment[0];
    }

    /**
     * Возвращает точку конца отрезка.
     */
    public static double[] getEndPoint(double[][] segment) {
        return segment[1];
    }

}
// END
