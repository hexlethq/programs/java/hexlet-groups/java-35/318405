// BEGIN
package exercise.geometry;
// END

public class Point {

    public static double[] makePoint(double x, double y) {
        double[] point = {x, y};
        return point;
    }

    public static double getX(double[] point) {
        return point[0];
    }

    public static double getY(double[] point) {
        return point[1];
    }

    // scheme for getQuadrant()
    /*
        2    |   1
        X -  |   X +
        Y +  |   Y +
      ---------------
        3    |   4
        X -  |   X +
        Y -  |   Y -
     */
    public static int getQuadrant(double[] point) {
        double pointX = Point.getX(point);
        double pointY = Point.getY(point);

        if (pointX > 0 && pointY > 0) {
            return 1;
        } else if (pointX < 0 && pointY > 0) {
            return 2;
        } else if (pointX < 0 && pointY < 0) {
            return 3;
        } else if (pointX > 0 && pointY < 0) {
            return 4;
        } else {
            return 0;
        }
    }

}

