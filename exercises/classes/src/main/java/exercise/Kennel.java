package exercise;

import java.util.Arrays;

public class Kennel {
    public static void main(String[] args) {
        // добавка щенка
        String[] newPuppy = {"Rex", "boxer"};
        addPuppy(newPuppy);
        System.out.println("addPuppy output:\n" + Arrays.deepToString(getAllPuppies()) + "\n");

        // добавка массива щенков
        String[][] puppies2 = {
                {"Rocky", "terrier"},
                {"Buddy", "chihuahua"},
                {"Toby", "chihuahua"},
                {"Ein", "corgi"},
        };
        Kennel.addSomePuppies(puppies2);
        System.out.println("addSomePuppies output:\n" + Arrays.deepToString(getAllPuppies()) + "\n");

        // кол-во щенков
        System.out.println("getPuppyCount:\n" + getPuppyCount() + "\n");

        // содержит ли массив выбранное имя
        System.out.println("isContainPuppy output:\n" + isContainPuppy("apchkhi") + "\n");

        // вывести массив щенков выбранной породы
        System.out.println("getNamesByBreed output:\n" + Arrays.deepToString(getNamesByBreed("chihuahua"))+ "\n");

        // Доп. задача: убрать щенка из питомник
        removePuppy("Rex");
        System.out.println("removePuppy:\n" + getPuppyCount());
        System.out.println(Arrays.deepToString(getAllPuppies()) + "\n");

        // очистка массива
        Kennel.resetKennel();
        System.out.println("resetKennel:\n" + Arrays.deepToString(getAllPuppies()));
        System.out.println("getPuppyCount:\n" + getPuppyCount() + "\n");
    }
    // FOR DEBUG

    private static String[][] allPuppies = new String[0][0];
    private static int puppiesCount = 0;

    /**
     * Добавляет щенка в питомник.
     */
    public static void addPuppy(String[] newPuppy) {
        allPuppies = Arrays.copyOf(allPuppies, puppiesCount + 1);
        allPuppies[puppiesCount] = newPuppy;
        puppiesCount++;
    }

    /**
     * Добавляет массив щенков в питомник.
     */
    public static void addSomePuppies(String[][] puppies) {
        for (String[] puppy : puppies) {
            addPuppy(puppy);
        }
    }

    /**
     * Возвращает общее количество щенков, находящихся на данный момент в питомнике.
     */
    public static int getPuppyCount() {
        return allPuppies.length;
    }

    /**
     * Cодержит ли массив выбранное имя.
     * Если щенок есть, метод возвращает `true`, в ином случае — `false`.
     */
    public static boolean isContainPuppy(String name) {
        for (String[] puppy : allPuppies) {
            if (puppy[0].equals(name)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Возвращает всех щенков, имеющихся в питомнике, в виде вложенного массива.
     * Щенки в массиве располагаются в том порядке, в каком они были добавлены в питомник.
     */
    public static String[][] getAllPuppies() {
        return allPuppies;
    }

    /**
     * Возвращает массив с именами щенков этой породы.
     */
    public static String[] getNamesByBreed(String breed) {
        String[] namesArr = new String[0];
        int matchCounter = 0;

        for (String[] puppyBreed : allPuppies) {
            if (puppyBreed[1].equals(breed)) {
                namesArr = Arrays.copyOf(namesArr, namesArr.length + 1);

                namesArr[matchCounter] = puppyBreed[0];
                matchCounter ++;
            }
        }

        return namesArr;
    }

    /**
     * Очищает питомник, не оставляя в нем ни одного щенка.
     */
    public static void resetKennel() {
        allPuppies = new String[0][0];
    }

    /**
     * Если щенок с таким именем есть в питомнике, метод удаляет его из питомника
     * и возвращает `true`. Если щенок отсутствует, метод просто возвращает `false`.
     */
    public static boolean removePuppy(String name) {
        if (isContainPuppy(name)) {
            // найти и обнулить (null) щенка
            int indexOfPuppy = 0;
            for (int i = 0; i < allPuppies.length; i++) {
                if (allPuppies[i][0].equals(name)) {
                    allPuppies[i][0] = null;
                    allPuppies[i][1] = null;
                    indexOfPuppy = i;
                    break;
                }
            }

            // убрать null элемент
            String[][] tmpArr = new String[allPuppies.length - 1][];
            System.arraycopy(allPuppies, 0,
                    tmpArr, 0, indexOfPuppy);
            System.arraycopy(allPuppies, indexOfPuppy + 1,
                    tmpArr, indexOfPuppy, allPuppies.length - indexOfPuppy - 1);
            allPuppies = tmpArr;

            return true;
        }

        return false;
    }
}