package exercise;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import io.javalin.Javalin;
import io.ebean.DB;
import io.ebean.Transaction;

import exercise.domain.User;
import exercise.domain.query.QUser;

class AppTest {

    private static Javalin app;
    private static String baseUrl;
    private static Transaction transaction;

    // BEGIN
    @BeforeAll
    public static void beforeAll() {
        app = App.getApp();
        app.start(0);
        int port = app.port();
        baseUrl = "http://localhost:" + port;
    }

    @AfterAll
    public static void afterAll() {
        app.stop();
    }
    // END

    // Хорошей практикой является запуск тестов с базой данных внутри транзакции.
    // Перед каждым тестом транзакция открывается,
    @BeforeEach
    void beforeEach() {
        transaction = DB.beginTransaction();
    }

    // А после окончания каждого теста транзакция откатывается
    // Таким образом после каждого теста база данных возвращается в исходное состояние,
    // каким оно было перед началом транзакции.
    // Благодаря этому тесты не влияют друг на друга
    @AfterEach
    void afterEach() {
        transaction.rollback();
    }

    @Test
    void testRoot() {
        HttpResponse<String> response = Unirest.get(baseUrl).asString();
        assertThat(response.getStatus()).isEqualTo(200);
    }

    @Test
    void testUsers() {

        // Выполняем GET запрос на адрес http://localhost:port/users
        HttpResponse<String> response = Unirest
            .get(baseUrl + "/users")
            .asString();
        // Получаем тело ответа
        String content = response.getBody();

        // Проверяем код ответа
        assertThat(response.getStatus()).isEqualTo(200);
        // Проверяем, что на станице есть определенный текст
        assertThat(content).contains("Wendell Legros");
        assertThat(content).contains("Larry Powlowski");
    }

    @Test
    void testUser() {

        HttpResponse<String> response = Unirest
            .get(baseUrl + "/users/5")
            .asString();
        String content = response.getBody();

        assertThat(response.getStatus()).isEqualTo(200);
        assertThat(content).contains("Rolando Larson");
        assertThat(content).contains("galen.hickle@yahoo.com");
    }

    @Test
    void testNewUser() {

        HttpResponse<String> response = Unirest
            .get(baseUrl + "/users/new")
            .asString();

        assertThat(response.getStatus()).isEqualTo(200);
    }

    // BEGIN

    @Test
    void testCreateUserValid() {
        HttpResponse<String> responsePost = Unirest
            .post(baseUrl + "/users")
            .field("firstName", "FirstName01")
            .field("lastName", "LastName01")
            .field("email", "email01@mail.ru")
            .field("password", "1234")
            .asString();

        User actualUser = new QUser()
            .email.equalTo("email01@mail.ru")
            .findOne();

        assertThat(responsePost.getStatus()).isEqualTo(302);
        assertThat(actualUser).isNotNull();

        assertThat(actualUser.getFirstName()).isEqualTo("FirstName01");
        assertThat(actualUser.getLastName()).isEqualTo("LastName01");
    }

    @Test
    void testCreateUserWithNotValidPassword() {
        HttpResponse<String> responsePost = Unirest
            .post(baseUrl + "/users")
            .field("firstName", "FirstName02")
            .field("lastName", "LastName02")
            .field("email", "email02@mail.ru")
            .field("password", "123")
            .asString();

        User actualUser = new QUser()
            .email.equalTo("email02@mail.ru")
            .findOne();

        String content = responsePost.getBody();

        assertThat(responsePost.getStatus()).isEqualTo(422);
        assertThat(actualUser).isNull();

        assertThat(content).contains("FirstName02");
        assertThat(content).contains("LastName02");
        assertThat(content).contains("email02@mail.ru");
        assertThat(content).contains("Пароль должен содержать не менее 4 символов");
    }

    @Test
    void testCreateUserWithNotValidName() {
        HttpResponse<String> responsePost = Unirest
            .post(baseUrl + "/users")
            .field("firstName", "")
            .field("lastName", "")
            .field("email", "email02@mail.ru")
            .field("password", "1234")
            .asString();

        User actualUser = new QUser()
            .email.equalTo("email02@mail.ru")
            .findOne();

        String content = responsePost.getBody();

        assertThat(responsePost.getStatus()).isEqualTo(422);
        assertThat(actualUser).isNull();

        assertThat(content).contains("");
        assertThat(content).contains("");
        assertThat(content).contains("email02@mail.ru");
        assertThat(content).contains("Имя не должно быть пустым");
        assertThat(content).contains("Фамилия не должна быть пустой");
    }

    @Test
    void testCreateUserWithNotValidEmail() {
        HttpResponse<String> responsePost = Unirest
            .post(baseUrl + "/users")
            .field("firstName", "FirstName02")
            .field("lastName", "LastName02")
            .field("email", "email02mail.ru")
            .field("password", "1234")
            .asString();

        String content = responsePost.getBody();

        assertThat(responsePost.getStatus()).isEqualTo(422);

        assertThat(content).contains("");
        assertThat(content).contains("");
        assertThat(content).contains("email02mail.ru");
        assertThat(content).contains("Должно быть валидным email");
    }
    // END
}
