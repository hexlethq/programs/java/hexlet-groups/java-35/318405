package exercise;
import java.sql.SQLOutput;
import java.util.Arrays;

class App {
//    // BEGIN
//    public static void main(String[] args) {
//        int[][] matrix = {
//                {1, 2, 33, 66},
//                {4, 5, 6}
//        };
//        System.out.println(flattenMatrix(matrix));
//    } // FOR DEBUG

    public static int[] reverse(int[] numbers) {
        int length = numbers.length;
        int[] result = new int[length];
        for (int i = length - 1; i >= 0; i--) {
            result[length - 1 - i] = numbers[i];
        }
        return result;
    }

    public static int mult(int[] numbers1) {
        int result = 1;
        for (int i = 0; i < numbers1.length; i++) {
            result = result * numbers1[i];
        }
        return result;
    }

    public static int[] flattenMatrix(int[][] matrix) {

        // finding matrix length:
        int matrixLength = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrixLength++;
            }
        }

        // matrix to array:
        int[] result = new int[matrixLength];
        int counterForAllLoop = 0;
        for (int[] row: matrix) {
            for (int element: row) {
                result[counterForAllLoop] = element;
                counterForAllLoop++;
            }
        }

        int[] emptyAr = {};
        if (matrixLength != 0) {
            return result;
        } else {
            return emptyAr;
        }
    }
    // END
}
