package exercise.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;

import static exercise.Data.getCompanies;

public class CompaniesServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        // BEGIN
        PrintWriter out = response.getWriter();

        List<String> companies = getCompanies();
        String queryString = request.getQueryString();
        String searchParameter = request.getParameter("search");

        String plainListOfSearchParameter = null;

        if (queryString == null || !queryString.contains("search") || searchParameter.isEmpty()) {
            companies.forEach(string -> out.println(string));
        }
        else {
            plainListOfSearchParameter = getPlainListOfSearchParameter(companies, searchParameter);
            plainListOfSearchParameter = getStubifThisListEmpty(plainListOfSearchParameter, "Companies not found");

            out.println(plainListOfSearchParameter);
        }

        out.close();
        // END
    }


    private String getPlainListOfSearchParameter(List<String> companies, String searchParameter) {
        String listOfSearchParameter = "";

        for (String company : companies) {
            if (company.contains(searchParameter)) {
                listOfSearchParameter += company + "\n";
            }
        }

        return listOfSearchParameter;
    }

    private String getStubifThisListEmpty(String plainList, String stub) {
        if (plainList.length() != 0) {
            return plainList;
        } else {
            return stub;
        }
    }
}
