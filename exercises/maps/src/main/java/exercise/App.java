package exercise;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Arrays;

// BEGIN
class App {
    public static void main(String[] args) {
        String sentence1 = "Bond never never never die";
        String sentence2 = " ";
        // Task 1
        System.out.println(getWordCount(sentence2));
        // Task 2
        Map wordsCount = App.getWordCount(sentence1);
        String result = App.toString(wordsCount);
        System.out.println(result);
    }

    /**
     * Make each word as list element
     */
    public static List<String> toList(String sentence) {
        String[] splitSentence = sentence.split(" ");
        return Arrays.asList(splitSentence);
    }

    /**
    * Task 1: Set count of each word in sentence
    */
    public static Map<String, Integer> getWordCount(String sentence) {
        List<String> words = toList(sentence);
        Map<String, Integer> wordsAndNumbers = new HashMap<>();

        if (sentence.equals("")) {
            return wordsAndNumbers;
        }

        for (String word: words) {
            int variableForValue = wordsAndNumbers.getOrDefault(word, 0) + 1;
            wordsAndNumbers.put(word, variableForValue);
        }

        return wordsAndNumbers;
    }

    /**
     * Task 2: From Map to formatted string
     */
    public static String toString(Map<String, Integer> wordsAndNumbers) {
        if (wordsAndNumbers.size() == 0) {
            return "{}";
        }

        String result = "{\n";
        for (String key: wordsAndNumbers.keySet()) {
            result = result + "  " + key + ": " + wordsAndNumbers.get(key) + "\n";
        }
        result = result + "}";

        return result;
    }

}
//END