package exercise;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// BEGIN
public class Validator {
    public static List<String> validate(Address address) {
        List<String> validateFields = new ArrayList<>();
        Field[] fields = address.getClass().getDeclaredFields();

        for (Field field : fields) {
            field.setAccessible(true);
            NotNull notNullAnnonation = field.getAnnotation(NotNull.class);
            Object fieldValue = getFieldValue(field, address);

            if (notNullAnnonation != null && fieldValue == null) {
                validateFields.add(field.getName());
            }
        }

        return validateFields;
    }

    public static Map<String, List<String>> advancedValidate(Address address) {
        Map<String, List<String>> validateFields = new HashMap<>();
        Field[] fields = address.getClass().getDeclaredFields();

        for (Field field : fields) {
            field.setAccessible(true);
            MinLength minLengthAnnotation = field.getAnnotation(MinLength.class);
            NotNull notNullAnnonation = field.getAnnotation(NotNull.class);

            Object fieldValue = getFieldValue(field, address);
            int fieldSize = getLengthIfNotNull(fieldValue);

            if (minLengthAnnotation != null && fieldSize < minLengthAnnotation.minLength()) {
                List<String> description1 = List.of("length less than " + minLengthAnnotation.minLength());
                validateFields.put(field.getName(), description1);
            }
            if (notNullAnnonation != null && fieldValue == null) {
                List<String> description2 = List.of("can not be null");
                validateFields.put(field.getName(), description2);
            }
        }

        return validateFields;
    }

    static Object getFieldValue(Field field, Address address) {
        Object fieldValue = null;
        try {
            fieldValue = field.get(address);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return fieldValue;
    }

    static int getLengthIfNotNull(Object field) {
        int fieldSize = 0;
        if (field != null) {
            fieldSize = field.toString().length();
        }
        return fieldSize;
    }
}
// END
