package exercise;
import java.util.Arrays;

class App {
    // BEGIN
    public static void main(String[] args) {
//        int[] numbers1 = {}; // -1
//        int[] numbers2 = {1, 2, 3, 4, 5}; // -1
//        int[] numbers3 = {-30, 42, -5, 31, -37, 25, -50}; // 2
//        int[] numbers4 = {-30, 42, -6, -5, -5, 31, -37}; // 3
//        int[] numbers5 = {-30, -42, 6, 5, 31, 3}; // 0
//        System.out.println(getIndexOfMaxNegative(numbers5));

//        int[] numbers1 = {};
//        int[] result1 = getElementsLessAverage(numbers1);
//        int[] numbers2 = {0, 1, 2, 3, 4, 5, 10, 12};
//        int[] result2 = getElementsLessAverage(numbers2); // [0, 1, 2, 3, 4]
//        int[] numbers3 = {1, 2, 6, 3, 8, 12};
//        int[] result3 = App.getElementsLessAverage(numbers3); // [1, 2, 3]
//        System.out.println(Arrays.toString(result3));

//        int[] numbers1 = {5, 4, 34, 8, 11, -5, 1}; // 19
//        int[] numbers2 = {7, 1, 37, -5, 11, 8, 1}; // 0
//        System.out.println(getSumBeforeMinAndMax(numbers1));
    }
    // FOR DEBUG

    public static int getIndexOfMaxNegative(int[] numbers) {
        int index = -1;
        for (int i = 0; i < numbers.length; i++) {
            if (index == -1 && numbers[i] <= index) { // first negative number
                index = i;
            } else if (index != -1 && numbers[i] < 0 && numbers[i] > numbers[index]) { // other negative numbers
                index = i;
            }
        }

        return index;
    }
    // END OF TASK

    public static int[] getElementsLessAverage(int[] numbers) {
        if (numbers.length == 0) {
            return numbers;
        }

        // sum of all elements
        int sum = 0;
        int numberOfAllElements = numbers.length;
        for (int element: numbers) {
            sum += element;
        }

        // selection elements that less average
        int average = sum / numberOfAllElements;
        int[] elementsLessAverage = new int[numbers.length];
        int counter = 0;
        for (int element: numbers) {
            if (element <= average) {
                elementsLessAverage[counter] = element;
                counter++;
            }
        }

        return Arrays.copyOfRange(elementsLessAverage, 0, counter);
    }
    // END OF TASK

    public static int getSumBeforeMinAndMax(int[] numbers) {
        int indexMax = 0;
        int indexMin = 0;
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] >= numbers[indexMax]) {
                indexMax = i;
            } else if (numbers[i] < numbers[indexMin]) {
                indexMin = i;
            }
        }

        // result sum
        int sum = 0;
        int getMaxIndex = Math.max(indexMin, indexMax);
        int getMinIndex = Math.min(indexMin, indexMax);
        for (int i = getMinIndex + 1; i < getMaxIndex; i++) {
            sum += numbers[i];
        }

        return sum;
    }
    // END OF TASK
}
