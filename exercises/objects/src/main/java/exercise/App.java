package exercise;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.time.LocalDate;

class App {
    public static void main(String[] args) {
//        String[] animals = {};
//        System.out.println(App.buildList(animals));
//
//        String[][] users = {
//                {"Andrey Petrov", "1990-11-23"},
//                {"Aleksey Ivanov", "1995-02-15"},
//                {"John Smith", "1990-03-11"},
//                {"Vanessa Vulf", "1985-11-16"},
//                {"Vladimir Nikolaev", "1990-12-27"},
//                {"Alice Lucas", "1986-01-01"},
//        };
//        System.out.println(App.getUsersByYear(users, 1990));
    }
    // FOR DEBUG

    // BEGIN
    public static String buildList(String[] strAr) {
        StringBuilder sb = new StringBuilder();

        if (strAr.length == 0) {
            return sb.toString();
        }

        sb.append("<ul>\n");
        for (String element: strAr) {
            sb.append("  <li>" + element + "</li>\n"); // почему IDEA ругается на append в этой строчке?
        }
        sb.append("</ul>");

        return sb.toString();
    }
    // END OF TASK

    public static String getUsersByYear(String[][] users, int year) {
        String[] usersByYear = new String[users.length];
        int matchCounter = 0;
        int matchNumberOfYear = 0;
        for (int i = 0; i < users.length; i++) {
            LocalDate date = LocalDate.parse(users[i][1]);
            int fromDateToYear = date.getYear();

            if (fromDateToYear == year) {
                usersByYear[matchCounter] = users[i][0];
                matchCounter++;
                matchNumberOfYear++;
            }
        }

        String[] usersByYearCopy = Arrays.copyOf(usersByYear, matchNumberOfYear);
        return App.buildList(usersByYearCopy);
    }
}
