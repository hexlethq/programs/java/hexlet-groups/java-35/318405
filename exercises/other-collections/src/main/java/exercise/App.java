package exercise;

import java.util.*;

// BEGIN
class App {
    public static LinkedHashMap<String, String> genDiff(Map<String, Object> data1, Map<String, Object> data2) {
        LinkedHashMap<String, String> resultDiff = new LinkedHashMap<>();

        Set<String> allData = new TreeSet<>();
        allData.addAll(data1.keySet());
        allData.addAll(data2.keySet());
        for (String key : allData) {
            resultDiff.put(key, checkDiffAndReturnNewKey(key, data1, data2));
        }

        return resultDiff;
    }

    public static String checkDiffAndReturnNewKey(String key, Map<String, Object> data1, Map<String, Object> data2) {
        if (data1.get(key) != null && data2.get(key) == null) {
            return "deleted";
        }else if (data1.get(key) == null && data2.get(key) != null) {
            return "added";
        } else if (data1.get(key).equals(data2.get(key))) {
            return "unchanged";
        } else {
            return "changed";
        }
    }
}
//END
